FROM ubuntu:16.04

LABEL description="Docker Django MySQL project template"
LABEL maintainer="Noel Andrei Elizaga <naaeliz@gmail.com>"

ENV LANG C.UTF-8
ENV CI 1
ENV PIPENV_NOSPIN 1

RUN apt-get -y update \
    && apt-get -y install \
            build-essential \
            gettext \
            nano \
            python-dev \
            python-pip \
            python3-dev \
            python3-pip \
            libmysqlclient-dev

RUN python2 -m pip install -U pip \
    && python2 -m pip install -U setuptools wheel \
    && python3 -m pip install -U pip \
    && python3 -m pip install -U setuptools wheel pipenv

COPY . /django_mysql_docker/
RUN cd /django_mysql_docker/ \
  && pipenv install --python $(which python2.7) --deploy --dev
